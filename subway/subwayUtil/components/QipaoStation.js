/**
 * 表示站点的类
 */
Subwaymap.QipaoStation = org.gridsofts.util.Class.define({
    name: "Subwaymap.QipaoStation",

    /**
     * 静态属性
     */
    statics: {
        // 普通站
		STATION: "station",
		// 换乘站
		TRANSFER: "transfer",
		// 三条线路换乘站

		TRANSFER3: "transfer3",
		// 半程车换乘站
		TRANSFER_HALF: "transfer_half"
    },

    /**
     * 构造函数
     */
    construct: function(container, station,qipao) {
		if (arguments.length == 0 || typeof(arguments[0]) != "object") {
			throw new Error("Object construct parameter is empty, unable to initialize instance of Station.");
		}

		this.container = container;
        this.station = station;

        // 站点图标半径
        this.radius = qipao.radius ? qipao.radius : 20;
        // 站点图标笔触
        this.thickness = station.radius ? station.radius / 3 : -1;
		
        // 首选颜色
		this.color1 = station.color && station.color.length > 0 ?
                        station.color: 0x0;

        this.repaint();
    },

    /**
     * 方法
     */
    methods: {
		repaint: function() {
			this.actualDrawStation();
		},

		/** ***********************************************************
		 * 调用绘图API，在画布上绘制图元
		 */
		actualDrawStation: function() {
			var code = this.station.code;
			var sprite = new Kinetic.Notice({
				x: this.station.x,
				y: this.station.y,
				name:  this.station.code,
				radius: this.radius,
				fill: "#" + this.color1,
				//stroke: "#" + this.color1,
				strokeWidth: this.thickness,
                opacity:0.6,
			});
			/* 添加监听事件*/
			if(!this.station.disabled){
				sprite.addEventListener("click",function(){
                    alert(code);
                    return false;
                });
			}
			this.container.add(sprite);
		}
    }
});