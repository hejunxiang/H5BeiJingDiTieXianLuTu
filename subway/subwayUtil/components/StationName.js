/**
 * Created by syy on 2017/6/26.
 */
/**
 * 表示站名的类
 */
Subwaymap.StationName = org.gridsofts.util.Class.define({
    name: "Subwaymap.StationName",

    /**
     * 构造函数
     */
    construct: function(container, station) {
        if (arguments.length == 0 || typeof(arguments[0]) != "object") {
            throw new Error("Object construct parameter is empty, unable to initialize instance of Station.");
        }

        this.container = container;
        this.station = station;

        this.color1=station.label.color  && station.label.color.length >0 ? station.label.color :station.color && station.color.length>0 ? station.color:"ffffff",
        this.repaint();
    },

    /**
     * 方法
     */
    methods: {
        repaint: function() {
            this.actualDrawStation();
        },

        /** ***********************************************************
         * 调用绘图API，在画布上绘制图元
         */
        actualDrawStation: function() {
            var text = new Kinetic.Text({
                x: this.station.x,
                y:  this.station.y,
                fontVariant: 'small-caps',
                text: this.station.label.text,
                align: 'center',
                fontSize: this.station.label.fontSize,
                fontFamily: 'Arial',
                fill:"#"+ this.color1,
                rotation:this.station.label.rotate,
            });
            var width = text.width();
            var heigth=text.height();
            var top=this.station.label.top;
            var bottom=this.station.label.bottom;
            var left=this.station.label.left;
            var right=this.station.label.right;
            var code = this.station.code;
            if(typeof (right) != "undefined" && null != right && typeof(top) != "undefined" && null !=  top ){  //右上
                text.x(text.x()+ right);
                text.y(text.y() - top - heigth);
            }else if(typeof (right) != "undefined" && null != right && typeof (bottom) != "undefined" && null !=  bottom){  //右下
                text.x(text.x()+ right);
                text.y(text.y()+ bottom);
            }else if(typeof (left) != "undefined" && null !=  left && typeof (bottom) != "undefined" && null !=  bottom){  //左下
                text.x(text.x() - left - width);
                text.y(text.y() + bottom);
            }else if(typeof (left) != "undefined" && null !=  left && typeof(top) != "undefined" && null !=  top){  //左上
                text.x(text.x() - left - width);
                text.y(text.y() - top - heigth);
            }else{
                if (typeof (top) != "undefined" && null != top) {
                    text.y(text.y() - top - heigth);
                    text.x(text.x() - width/2);
                }
                if (typeof (bottom) != "undefined" && null !=  bottom) {
                    text.y(text.y()  + bottom);
                    text.x(text.x() - width/2);  //左右居中
                }
                if (typeof (left) != "undefined" && null !=  left) {
                    text.x(text.x() - left - width);
                    text.y(text.y() - heigth/2);
                }
                if (typeof (right) != "undefined" && null !=  right) {
                    text.x(text.x() + right);
                    text.y(text.y() - heigth/2);  //上下居中
                }
                if (typeof ( this.station.label.rotate) != "undefined" && null !=  this.station.label.rotate) {
                    text.rotation(this.station.label.rotate);
                }
            }
            /* 添加监听事件*/
			if(!this.station.disabled){
				text.addEventListener("click",function(){
                    alert(code);
                    return false;
                });
			}
            this.container.add(text);
        },
    }
});