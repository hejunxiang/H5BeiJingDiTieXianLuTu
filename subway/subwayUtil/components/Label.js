/**
 * 用于显示站点名称的文字标签类
 */
Subwaymap.Label = org.gridsofts.util.Class.define({
    name: "Subwaymap.Label",

    /**
     * 构造函数
     */
    construct: function(station) {
		if (arguments.length == 0 || typeof(arguments[0]) != "object") {
			throw new Error("Object construct parameter is empty, unable to initialize instance of Label.");
		}

        this.station = station;
        
        this.repaint();
    },

    /**
     * 方法
     */
    methods: {

		getSpriteComponent: function() {
			return this.sprite;
		},

		repaint: function() {

			this.sprite = new Kinetic.Text({
                x: x,
                y: y,
                fontVariant: 'small-caps',
                text: label,
                align: 'center',
                fontSize: size,
                fontFamily: 'Arial',
                fill: '#' + fill
            });
		},
		
		/**
		 * 普通站
		 */
		drawStation: function(color) {
			console.log(color);
			
			// var circle:Sprite = new Sprite();
			// addChild(circle);
			
			// // 圆环
			// if (disabled) {
			// 	circle.graphics.lineStyle(thickness == -1 ? 4.5 : thickness, 0xcccccc);
			// 	circle.graphics.beginFill(0xfafafa);
			// } else {
			// 	circle.graphics.lineStyle(thickness == -1 ? 4.5 : thickness, color);
			// 	circle.graphics.beginFill(0xfafafa);
			// }
			
			// circle.graphics.drawCircle(0, 0, radius);
			// circle.graphics.endFill();
		},
    }
});