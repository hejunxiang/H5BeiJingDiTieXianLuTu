/**
 * Created by syy on 2017/6/26.
 */
/**
 * 表示 站点上 所属线路
 */
Subwaymap.StationCode = org.gridsofts.util.Class.define({
    name: "Subwaymap.StationCode",

    /**
     * 构造函数
     */
    construct: function(container, station) {
        if (arguments.length == 0 || typeof(arguments[0]) != "object") {
            throw new Error("Object construct parameter is empty, unable to initialize instance of Station.");
        }

        this.container = container;
        this.station = station;

        this.color1=station.color  && station.color.length >0 ? station.color : "000000";
        var temp= station.code ? (station.code).substring(0,2) : "换";
        this.text=this.stationCode(temp);
        this.repaint();
    },

    /**
     * 方法
     */
    methods: {
        repaint: function() {
            this.actualDrawStation();
        },
        stationCode:function(text){
            if("01"==text){
                text="1";
            }else if("02"==text){
                text="2";
            }else if("03"==text){
                text="3";
            }else if("04"==text){
                text="4";
            }else if("05"==text){
                text="5";
            }else if("06"==text){
                text="6";
            }else if("07"==text){
                text="7";
            }else if("08"==text){
                text="8";
            }else if("09"==text){
                text="9";
            }else if("10"==text){
                text="10";
            }else if("13"==text){
                text="13";
            }else if("14"==text){
                text="14";
            }else if("15"==text){
                text="15";
            }else if("16"==text){
                text="16";
            }else if("YZ"==text){
                text="亦";
            }else if("FS"==text){
                text="房";
            }else if("AE"==text){
                text="机";
            }else if("CP"==text){
                text="昌";
            }else if("BT"==text){
                text="通";
            }else if("18"==text){
                text="18";
            }else{
                 text="0";
            }
            return text;
        },

        /** ***********************************************************
         * 调用绘图API，在画布上绘制图元
         */
        actualDrawStation: function() {
            var code = this.station.code;
            var text = new Kinetic.Text({
                x: this.station.x,
                y:  this.station.y,
                fontVariant: 'small-caps',
                text: this.text,
                align: 'center',
                fontSize: this.station.label.fontSize,
                fontFamily: 'Arial',
                fill:"#"+ this.color1,
            });
            var width = text.width();
            var heigth=text.height();
            text.y(text.y()  - heigth/2); //上下居中
            text.x(text.x() - width/2);  //左右居中
            /* 添加监听事件*/
			if(!this.station.disabled){
				text.addEventListener("click",function(){
                    alert(code);
                    return false;
                });
			}
            this.container.add(text);
        },
    }
});