/**
 * 线路图层(单线)类
 */
Subwaymap.LineLayer = org.gridsofts.util.Class.define({
	name:	"Subwaymap.LineLayer",
	
	/**
	 * 构造函数
	 */
	construct: function(container, line) {
		if (container==null || container==undefined || line==null || line==undefined) {
			throw new Error("Object construct parameter is empty, unable to initialize instance of LineLayer.");
		}

		// 处理参数
		this.container = container;
		this.line = line;

        this.layer = new Kinetic.Layer({
            id: "linelayer_" + line.code
        });
		this.container.add(this.layer);
        
        this.repaint();
	},
		
	methods: {
        /**
		 * 根据路网图数据，重绘路网线路
		 */
        repaint: function() {
			this.layer.clear();

			//绘制线路
			this.drawBranch(this.line);
        },

		drawBranch: function(line) {
            var primary=line.primary  //主线路
            var upLines=line.up;   //上行
            var downLines=line.down  //下行

            // 拼接用于画线的路径
            var primaryBranchPathData = "";
            if(primary!=null && primary.sections.length>0){
                var primarySections=primary.sections;
                for(var i=0;i<primarySections.length;i++){
                    primaryBranchPathData += primarySections[i].path;
                }
                this.actualDrawPath(primaryBranchPathData, line.code, primary.weight, primary.color,primary.alpha)
            }
            var upBranchPathData="";
            if(upLines!=null && upLines.sections.length>0){
                var upSections=upLines.sections;
                for(var i=0;i<upSections.length;i++){
                    upBranchPathData += upSections[i].path;
                }
                this.actualDrawPath(upBranchPathData, line.code, upLines.weight, upLines.color,upLines.alpha)
            }

            var downBranchPathData="";
            if(downLines!=null && downLines.sections.length>0){
                var downSections=downLines.sections;
                for(var i=0;i<downSections.length;i++){
                    downBranchPathData += downSections[i].path;
                }
                this.actualDrawPath(downBranchPathData, line.code, downLines.weight, downLines.color,downLines.alpha)
            }

            // 更新地图实际尺寸
			/*	if (currentStation.x > Subwaymap.actualWidth) {
			 Subwaymap.actualWidth = currentStation.x;
			 }else{
			 Subwaymap.minWidth=currentStation.x;
			 }
			 if (currentStation.y > Subwaymap.actualHeight) {
			 Subwaymap.actualHeight = currentStation.y;
			 }else{
			 Subwaymap.minHeight=currentStation.y;
			 }*/
		},
		// End: drawBranch
		/** ***********************************************************
		 * 调用绘图API，在画布上绘制图元
		 */
		actualDrawPath: function(branchPathData, name, strokeWidth, strokeColor,alpha) {
			
			var path = new Kinetic.Path({
				data: branchPathData,
				name: name,
				lineCap: "round",
				stroke: "#"+strokeColor,
				strokeWidth: strokeWidth,
                opacity:alpha
			});
			this.layer.add(path);
		},

		// End: drawStations
	}
});



