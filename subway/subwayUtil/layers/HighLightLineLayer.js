var fontSize=30;
/**
 *  高亮 线路图层 类
 */
Subwaymap.HighLightLineLayer = org.gridsofts.util.Class.define({
	name:	"Subwaymap.HighLightLineLayer.js",
	
	/**
	 * 构造函数
	 */
	construct: function(container, line) {
		if (arguments.length < 2 || typeof(arguments[1]) != "object" 
			|| !arguments[1].path || arguments[1].path.length == 0) {
			throw new Error("Object construct parameter is empty, unable to initialize instance of LineLayer.");
		}

		// 处理参数
		this.container = container;
		this.line = line;

        this.layer = new Kinetic.Layer({
            id: "linelayer_" + line.code
        });
		this.container.add(this.layer);
        
        this.repaint();
	},
		
	methods: {
        /**
		 * 根据路网图数据，重绘路网线路
		 */
        repaint: function() {
            var specialColor = arguments[0] ? arguments[0] : 0;
            var offset = arguments[1] ? arguments[1] : 0;

			this.layer.clear();
			
				// 绘制主线
			if (this.line.path && this.line.path.length > 0) {
				this.drawBranch(this.line, specialColor, offset);
			}
        },

		drawBranch: function(branch) {

			// 线颜色
            var specialColor = arguments.length > 1 && arguments[1] ? arguments[1] : 0;
            var offset = arguments.length > 2 && arguments[2] ? arguments[2] : 0;

			// 当前站点
			var currentStation = branch.path[0];
			// 控制点、下一个点
			var controlPoint, nextStation;

			// 拼接用于画线的路径
			var branchPathData = "";

			// 遍历所有点
			for (var i = 1; i < branch.path.length; ++i, currentStation = nextStation) {
				nextStation = branch.path[i];
			
				// 检查站点是否有单独的颜色信息
				if (currentStation.colors && currentStation.colors.length > 0) {
					// 如果有，则覆盖线路默认颜色
					specialColor = specialColor == 0x0 ? currentStation.colors[0] : specialColor;
				} else {
					// 设置线路颜色(标准色)
					specialColor = specialColor == 0x0 ? branch.color : specialColor;
				}
			
				// 移动画刷至线路起点
				branchPathData = Subwaymap.utils.obtainPathCoord("M", 
							(currentStation.x - Subwaymap.offsetX) + offset, 
							(currentStation.y - Subwaymap.offsetY) + offset);
				// 判断下一个点是否是曲线控制点
				// 如果是，则使用该控制点绘制曲线，同时跳过该点
				if (/^\s*control\s*/i.test(nextStation.type) && i < branch.path.length - 1) {
					controlPoint = nextStation;
					
					// 取出下一个站点信息，同时跳过控制点
					nextStation = branch.path[++i];
					
					// 绘制二次贝塞尔曲线
					branchPathData += Subwaymap.utils.obtainPathCoord("Q", 
							(controlPoint.x - Subwaymap.offsetX) + offset, 
							(controlPoint.y - Subwaymap.offsetY) + offset, 
							(nextStation.x - Subwaymap.offsetX) + offset, 
							(nextStation.y - Subwaymap.offsetY) + offset);

				} else {
					
					// 绘制直线
					branchPathData += Subwaymap.utils.obtainPathCoord("L", 
							(nextStation.x - Subwaymap.offsetX) + offset, 
							(nextStation.y - Subwaymap.offsetY) + offset);
				}
				
				// 调用绘图API，在画布上绘制图元
				this.actualDrawPath(branchPathData, this.line.code, branch.weight, "#" + specialColor);
				
				// 绘制分支
				if (currentStation.branch && currentStation.branch.length > 0) {
					this.drawBranch({
						weight: branch.weight,
						color: branch.color,
						path: currentStation.branch
					});
				}

				// 更新地图实际尺寸
			/*	if (currentStation.x > Subwaymap.actualWidth) {
					Subwaymap.actualWidth = currentStation.x;
				}else{
					Subwaymap.minWidth=currentStation.x;
				}
				if (currentStation.y > Subwaymap.actualHeight) {
					Subwaymap.actualHeight = currentStation.y;
				}else{
                    Subwaymap.minHeight=currentStation.y;
				}*/
			}
		},
		// End: drawBranch

		drawStations: function(branch) {

			// 线宽
			var specialWeight = branch.weight;
			// 线颜色
            var specialColor = branch.color;

			// 遍历所有点
			for (var i = 0; i < branch.path.length; ++i) {
				currentStation = branch.path[i];

				// 检查站点是否有单独的颜色信息
				if (currentStation.colors && currentStation.colors.length > 0) {
					// 如果有，则覆盖线路默认颜色
					specialColor = specialColor == 0x0 ? currentStation.colors[0] : specialColor;
				} else {
					// 设置线路颜色(标准色)
					specialColor = specialColor == 0x0 ? branch.color : specialColor;
				}

				// 绘制站点
				if (/^\s*station\s*/i.test(currentStation.type)) {
					new Subwaymap.Station(this.layer, currentStation, specialWeight, specialColor);
				}

				// 绘制分支
				if (currentStation.branch && currentStation.branch.length > 0) {
					this.drawStations({
						weight: specialWeight,
						color: specialColor,
						path: currentStation.branch
					});
				}
			}

			// 绘制最后一个站点
			if (/^\s*station\s*/i.test(currentStation.type)) {
				new Subwaymap.Station(this.layer, currentStation, specialWeight, specialColor);
			}
		},
        drawStationName:function(lines){
            // 遍历所有点

            for (var i = 0; i < lines.path.length; ++i) {
                var currentStation=lines.path[i];
                // 绘制站点
                if (/^\s*station\s*/i.test(currentStation.type)&&currentStation.visible) {
                   new Subwaymap.StationName(this.layer,currentStation);
                }
            }

		},
		/** ***********************************************************
		 * 调用绘图API，在画布上绘制图元
		 */
		actualDrawPath: function(branchPathData, name, strokeWidth, strokeColor) {
			
			var path = new Kinetic.Path({
				data: branchPathData,
				name: name,
				lineCap: "round",
				stroke: strokeColor,
				strokeWidth: strokeWidth
			});
			this.layer.add(path);
		},

		// End: drawStations
	}
});



