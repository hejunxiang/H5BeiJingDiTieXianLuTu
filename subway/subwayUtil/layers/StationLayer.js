/**
 * 站点图层类
 */
Subwaymap.StationLayer = org.gridsofts.util.Class.define({
	name:	"Subwaymap.StationLayer",
	
	/**
	 * 构造函数
	 */
    construct: function(container,stations,marked) {
        if (container==null || stations==null) {
            throw new Error("Object construct parameter is empty, unable to initialize instance of LineLayer.");
        }

		// 处理参数
        this.container = container;
		this.stations = stations;

		this.marked= marked;

        this.layer = new Kinetic.Layer({
            id: "stationlayer"
        });
        this.container.add(this.layer);

        this.repaint();
	},
		
	methods: {
        drawStations: function(stations) {
            for(var i=0;i<stations.length;i++){
                var station=stations[i];
                if(station.visible){
                    new Subwaymap.Station(this.layer, station);
                }
                if(this.marked!="highLight"){

                    // 更新地图实际尺寸
                    if (station.x > Subwaymap.actualWidth) {
                        Subwaymap.actualWidth = station.x;
                    }
                    if(station.x < Subwaymap.minWidth){
                        Subwaymap.minWidth=station.x;
                    }
                    if (station.y > Subwaymap.actualHeight) {
                        Subwaymap.actualHeight = station.y;
                    }
                    if(station.y < Subwaymap.minHeight){
                        Subwaymap.minHeight=station.y;
                    }

                }
            }
        },
        drawStationName:function(stations){
            //绘制站点名称
            for(var i=0;i<stations.length;i++){
                var station=stations[i];
                if(station.visible) {
                    new Subwaymap.StationName(this.layer, station);
                }
            }
        },
        drawStationCode:function(stations){
            //绘制站点线路code
            for(var i=0;i<stations.length;i++){
                var station=stations[i];
                if(station.visible) {
                    new Subwaymap.StationCode(this.layer, station);
                }
            }
        },
        /**
		 * 根据路网图数据，重绘路网线路
		 */
        repaint: function() {
			this.layer.clear();

            this.stations = arguments[0] ? arguments[0] :  this.stations;
            this.marked = arguments[1] ? arguments[1] : this.marked;
            // 绘制站点
            if (this.stations && this.stations.length > 0) {
                this.drawStations(this.stations);
                this.drawStationCode(this.stations);
                this.drawStationName(this.stations);
            }
        }
        // End: repaint

	}
});

