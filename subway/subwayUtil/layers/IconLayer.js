var fontSize=30;
/**
 * 线路图层(单线)类
 */
fontSize=60;
Subwaymap.IconLayer = org.gridsofts.util.Class.define({
	name:	"Subwaymap.IconLayer",
	
	/**
	 * 构造函数
	 */
	construct: function(container, icons) {
		if (container==null || icons==null) {
			throw new Error("Object construct parameter is empty, unable to initialize instance of LineLayer.");
		}

		// 处理参数
		this.container = container;
		this.icons = icons;

        this.layer = new Kinetic.Layer({
            id: "icons"
        });
		this.container.add(this.layer);

		this.repaint();
	},
		
	methods: {
	    repaint:function(){
            this.icons = arguments[0] ? arguments[0] : this.icons;
            for(var i=0;i<this.icons.length;i++){
                var icon=this.icons[i];
                if('label'==icon.type){
                    this.repaintText(icon);
                }else if("image"==icon.type){
                    this.repaintImg(icon);
                }else if("sprite"==icon.type){
                    this.repaintGraphic(icon);
                }
            }
        },
        repaintImg: function(icon) {
            var imageObj = new Image();
            imageObj.src = icon.imageurl;
            /** ***********************************************************
             * 调用绘图API，在画布上绘制图元
             */
            var image = new Kinetic.Image({
                x: icon.x,
                y: icon.y,
                image: imageObj,
            });
            this.layer.add(image);
        },
        repaintText:function(icon){
            var text = new Kinetic.Text({
                x: icon.x,
                y: icon.y,
                rotation:icon.rotate,
                fontVariant:'small-caps',
                text:  icon.label,
                align:'center',
                fontSize: icon.fontSize,
                fontFamily: 'Microsoft Yahei',//改成了微软雅黑会清晰一点
                fill: "#"+icon.color
            });
        },
        repaintGraphic:function(icon){
            var path = new Kinetic.Path({
                x: icon.x,
                y: icon.y,
                data:icon.path,
                lineCap: "round",
                stroke: "#"+icon.color,
                strokeWidth:icon.weight,
                opacity:icon.alpha
            });
            this.layer.add(path);
        }
	}
});



