/**
 * 站 流量监控 图层
 */
Subwaymap.StationLljkLayer = org.gridsofts.util.Class.define({
	name:	"Subwaymap.StationLljkLayer",
	
	/**
	 * 构造函数
	 */
    construct: function(container,cutpoint,lines) {
        if (arguments.length < 2 || typeof(arguments[1]) != "object" || lines==null) {
            throw new Error("Object construct parameter is empty, unable to initialize instance of LineLayer.");
        }


		// 处理参数
        this.container = container;
        this.lines=lines;
		this.cutpoint = cutpoint;

        this.layer = new Kinetic.Layer({
            id: "stationLljkLayer"+cutpoint.sectionId,
        });
        this.container.add(this.layer);

        this.repaint();
	},

	methods: {
        getLayer: function() {
            return this.layer;
        },
        /**
		 * 根据路网图数据，重绘路网线路
		 */
        repaint: function() {
			this.layer.clear();

            // 绘制断面
            this.drawCutPoint(this.cutpoint);
        },
        actualDrawPath: function(branchPathData, name, strokeWidth, strokeColor) {
            var path = new Kinetic.Path({
                data: branchPathData,
                name: name,
                lineCap: "round",
                stroke: strokeColor,
                strokeWidth: strokeWidth
            });
            this.layer.add(path);
        },
        drawBranch: function(cutpoints,cutpoint,weight) {
            // 拼接用于画线的路径
            var branchPathData = "";
            for(var i=0;i<cutpoints.length;i++){
                var updown=cutpoints[i];
                branchPathData += updown.path;
            }
            // 调用绘图API，在画布上绘制图元
            this.actualDrawPath(branchPathData, cutpoint.sectionId,weight, cutpoint.color);
        },
        drawCutPoint:function(cutpoint){
            var direction=cutpoint.direction;
            var beginCode=cutpoint.beginCode;
            var endCode=cutpoint.endCode;
            var weight=20;

            if(this.lines && this.lines.length>0){
                for(var i=0;i<this.lines.length;i++){
                    var upDownline= direction==1 ? this.lines[i].up : direction==2 ? this.lines[i].down : null;
                    weight=upDownline.weight;
                    var sections=upDownline.sections;
                    var cutpoints=new Array();  //一条线画一次
                    for(var j=0;sections && sections.length>0 && j<sections.length;j++){
                        var section=sections[j];
                        if(endCode==section.endCode){
                            cutpoints.push(section);
                            break;
                        }
                        if(beginCode==section.beginCode || (cutpoints!=null && cutpoints.length>0)){
                            cutpoints.push(section);
                        }
                    }
                    if(cutpoints && cutpoints.length>0){
                        //一条线画一次 断面
                        this.drawBranch(cutpoints,cutpoint,weight);
                    }
                }
            }
        }
	}
});

