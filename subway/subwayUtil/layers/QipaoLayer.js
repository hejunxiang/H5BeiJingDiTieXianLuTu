/**
 * 站点 气泡层
 */
var radius;
Subwaymap.QipaoLayer = org.gridsofts.util.Class.define({
	name:	"Subwaymap.QipaoLayer",
    layer:null,

	/**
	 * 构造函数
	 */
    construct: function(container,qipaos,stations) {
        if (container==null || stations.length<0 || qipaos==null) {
            throw new Error("Object construct parameter is empty, unable to initialize instance of LineLayer.");
        }

		// 处理参数
        this.container = container;
		this.stations = stations;
        this.qipaos=qipaos;

        this.layer = new Kinetic.Layer({
            id: "qipaoLayer"
        });
        this.container.add(this.layer);

        this.repaint();
	},
		
	methods: {

        getLayer: function() {
            return this.layer;
        },
        /**
		 * 根据路网图数据，重绘路网线路
		 */
        repaint: function() {
            this.layer.destroyChildren();

            this.layer.clear();

            this.qipaos = arguments[0] ? arguments[0] : this.qipaos;
            this.stations = arguments[1] ? arguments[1] :  this.stations;

            for(var i=0 ;i<this.qipaos.length;i++){
                var qipao=this.qipaos[i];
                for(var j =0;j<this.stations.length;j++){
                    var station=this.stations[j];
                    if(qipao.stationCode==station.code){
                        new Subwaymap.QipaoStation(this.layer, station,qipao);

                      /*  // 站点图标半径
                        var radius = qipao.radius ? qipao.radius : 20;
                        // 站点图标笔触
                        var thickness = station.radius ? station.radius / 3 : -1;

                        // 首选颜色
                        var color1 = station.color && station.color.length > 0 ? station.color: 0x0;
                        this.actualDrawStation(station,radius,color1,thickness);*/
                    }
                }
            }
        },
        // End: repaint
        //
        /** ***********************************************************
         * 调用绘图API，在画布上绘制图元
         */
        actualDrawStation: function(station,radius,color1,thickness){
            var sprite = new Kinetic.Circle({
                x: station.x,
                y: station.y,
                name:  station.code,
                radius: radius,
                fill: "#" + color1,
                stroke: "#" + color1,
                strokeWidth: thickness,
                opacity:0.6,
            });

            this.layer.add(sprite);
        }
	}
});

